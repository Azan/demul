#ifndef __DEMUL_MAIN_H__
#define __DEMUL_MAIN_H__

#define MEMSIZE 67108864

typedef unsigned int dword;
typedef unsigned short word;
typedef unsigned char byte;

typedef struct{
	dword A;	//Accumulator
	dword C;	//Counter
	dword X;	//auxillary register X
	dword Y;	//auxillary register Y
	dword IP;	//Instruction Pointer
	byte FLAG;	//juicy meaty byte of FLAGs
} cpureg;

extern cpureg reg;

void exit_status(void);
void opcode_interpret(byte opc);

extern byte *mem;

#endif
