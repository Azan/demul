## Demul - The made-for-own-amusement fictitious cpu emulator, where D stands for DWORD

To compile use `make`.
For now the only way to program this shiny beast of a virtual machine you have to make do with:  
`echo -e -n "[your chain-of\xnn's-here]" > [programname].dxf`  
The dxf extension is not mandatory, it just stands for Demul eXecutable File.  
To run your new masterpiece you execute  
`./demul [programname].dxf`  

When writing you masterpieces, watch out for the fact, that every integer must be given in DWORD, else the VM will confuse data and instructions.  
List of available operations is contained within opcode_interpret.c  
This readme is ugly because I can't markdown.  
Edit: it's not that ugly anymore.
