#include <stdio.h>
#include <stdlib.h>
#include "main.h"

cpureg reg;

byte *mem = NULL;

int main(int argc, const char **argv){
	mem = calloc(MEMSIZE, 1);
	FILE *srcbin = fopen(argv[1], "r");
	if(srcbin == NULL){
		fprintf(stderr, "Supervisor interrupt: Faulty file\n");
		exit(256);
	}else{
		fseek(srcbin, 0, SEEK_END);
		unsigned int filesize = ftell(srcbin);
		if(filesize > MEMSIZE){
			fprintf(stderr, "Supervisor interrupt: Memory limit exceeded\n");
			exit(256);
		}else{
			fseek(srcbin, 0, SEEK_SET);
			unsigned int i=0;
			fread(mem, 1, filesize, srcbin);
			fclose(srcbin);
		}
	}
	unsigned int i=0;
	for(i; i<=32; i++){
		printf("0x%08X ", mem[i]);
	}
	printf("\n");
	while(1){
		opcode_interpret(mem[reg.IP]);
	}
	return 0;
}
