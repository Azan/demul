#include <stdio.h>
#include <stdlib.h>
#include "main.h"

extern cpureg reg;

dword tmp=0;

void exit_status(void){
	printf("Exit status:\n");
	printf("A: 0x%08X\tC: 0x%08X\n", reg.A, reg.C);
	printf("X: 0x%08X\tY: 0x%08X\n", reg.X, reg.Y);
	printf("IP: 0x%08X\n", reg.IP);
	printf("FLAG: 0x%02X\n", reg.FLAG);
}

void check_zf(void){
	reg.A == 0 ? reg.FLAG |= 1 : (reg.FLAG &= ~1);
}

void check_nf(void){
	(reg.A >> 31) & 1 ? reg.FLAG |= (1 << 1) : (reg.FLAG &= ~(1 << 1));
}

void opcode_interpret(byte opc){
	reg.IP++;
	if(opc == 0x00){		//nop - no operation
	}else if(opc == 0x01){		//hlt - halt machine and show register values
		exit_status();
		exit(reg.A);
	}else if(opc == 0x02){		//lda iD - load immediate DWORD into register A
		reg.A ^= reg.A;
		reg.A = (((byte)mem[reg.IP++] << 24) |
			 ((byte)mem[reg.IP++] << 16) |
			 ((byte)mem[reg.IP++] << 8) |
			 ((byte)mem[reg.IP++]));
	}else if(opc == 0x03){		//ldx - load register A into register X
		reg.X = reg.A;
	}else if(opc == 0x04){		//ldy - load register A into register Y
		reg.Y = reg.A;
	}else if(opc == 0x05){		//ldc - load register A into register C
		reg.C == reg.A;
	}else if(opc == 0x06){		//swx - switch values of registers A and X
		reg.X ^= reg.A;
		reg.A ^= reg.X;
		reg.X ^= reg.A;
	}else if(opc == 0x07){		//swy - switch values of registers A and Y
		reg.Y ^= reg.A;
		reg.A ^= reg.Y;
		reg.Y ^= reg.A;
	}else if(opc == 0x08){		//swc - switch values of registers A and C
		reg.C ^= reg.A;
		reg.A ^= reg.C;
		reg.C ^= reg.A;
	}else if(opc == 0x09){		//sax - switch auxillary registers
		reg.X ^= reg.Y;
		reg.Y ^= reg.X;
		reg.X ^= reg.Y;
	}else if(opc == 0x0a){		//rtx - retrieve value of X to A
		reg.A = reg.X;
	}else if(opc == 0x0b){		//rty - retrieve value of Y to A
		reg.A = reg.Y;
	}else if(opc == 0x0c){		//rtc - retrieve value of C to A
		reg.A = reg.C;
	}else if(opc == 0x10){		//add iD - add immediate DWORD to reg.A
		reg.A += (((byte)mem[reg.IP++] << 24) |
			  ((byte)mem[reg.IP++] << 16) |
			  ((byte)mem[reg.IP++] << 8) |
			  ((byte)mem[reg.IP++]));
	}else if(opc == 0x11){		//sub iD - sub immediate DWORD to reg.A
		reg.A -= (((byte)mem[reg.IP++] << 24) |
			  ((byte)mem[reg.IP++] << 16) |
			  ((byte)mem[reg.IP++] << 8) |
			  ((byte)mem[reg.IP++]));
	}else if(opc == 0x12){		//mul iD - mul immediate DWORD to reg.A
		reg.A *= (((byte)mem[reg.IP++] << 24) |
			  ((byte)mem[reg.IP++] << 16) |
			  ((byte)mem[reg.IP++] << 8) |
			  ((byte)mem[reg.IP++]));
	}else if(opc == 0x14){		//div iD - div immediate DWORD to reg.A
		reg.A /= (((byte)mem[reg.IP++] << 24) |
			  ((byte)mem[reg.IP++] << 16) |
			  ((byte)mem[reg.IP++] << 8) |
			  ((byte)mem[reg.IP++]));
	}else if(opc == 0x15){		//mod iD - div immediate DWORD to reg.A
		reg.A %= (((byte)mem[reg.IP++] << 24) |
			  ((byte)mem[reg.IP++] << 16) |
			  ((byte)mem[reg.IP++] << 8) |
			  ((byte)mem[reg.IP++]));
	}else if(opc == 0x16){		//addx - add reg.X to reg.A
		reg.A += reg.X;
	}else if(opc == 0x17){		//subx - sub reg.X from reg.A
		reg.A -= reg.X;
	}else if(opc == 0x18){		//mulx - multiply reg.A by reg.X
		reg.A *= reg.X;
	}else if(opc == 0x19){		//divx - divide reg.A by reg.X
		reg.A /= reg.X;
	}else if(opc == 0x1a){		//modx - A modulo X
		reg.A %= reg.X;
	}else if(opc == 0x1b){		//addy - add reg.Y to reg.A
		reg.A += reg.Y;
	}else if(opc == 0x1c){		//suby - sub reg.Y from reg.A
		reg.A -= reg.Y;
	}else if(opc == 0x1d){		//muly - multiply reg.A by reg.Y
		reg.A *= reg.Y;
	}else if(opc == 0x1e){		//divy - divide reg.A by reg.Y
		reg.A /= reg.Y;
	}else if(opc == 0x1f){		//mody - A modulo Y
		reg.A %= reg.Y;
	}else if(opc == 0x20){		//inca - increment A
		reg.A++;
	}else if(opc == 0x21){		//deca - decrement A
		reg.A--;
	}else if(opc == 0x22){		//incx - increment X
		reg.X++;
	}else if(opc == 0x23){		//decx - decrement X
		reg.X--;
	}else if(opc == 0x24){		//incy - increment Y
		reg.Y++;
	}else if(opc == 0x25){		//decy - decrement Y
		reg.Y--;
	}else if(opc == 0x26){		//cu - increment C (count up)
		reg.C++;
	}else if(opc == 0x27){		//cd - decrement C (count down)
		reg.C--;
	}else if(opc == 0x28){		//na - make A negative
		reg.A |= (1 << 31);
	}else if(opc == 0x29){		//nx - make X negative
		reg.X |= (1 << 31);
	}else if(opc == 0x2a){		//ny - make Y negative
		reg.Y |= (1 << 31);
	}else{
		fprintf(stderr, "Supervisor interrupt: Invalid opcode or Machine not terminated\n");
		exit(255);
	}
	check_zf();
	check_nf();
}
