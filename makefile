$(CC) = gcc

all:
	$(CC) main.c opcode_interpret.c -o demul

example:
	echo -e -n "\x02\x00\x00\x00\x02\x03\x02\x00\x00\x00\x01\x06\x01" > example.dxf

clean:
	rm -f demul
	rm -f example.dxf
